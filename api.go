package main

import (
	"example/go-orm-api/model"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root@tcp(127.0.0.1:3306)/go_orm?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	r := gin.Default()

	// GET method ---------------------------------------------------------------------
	r.GET("/users", func(c *gin.Context) {
		var users []model.User
		db.Find(&users)
		c.JSON(http.StatusOK, users)
	})

	// GET:id method ------------------------------------------------------------------
	r.GET("/users/:id", func(c *gin.Context) {
		id := c.Param("id")
		var user model.User
		db.First(&user, id)
		c.JSON(http.StatusOK, user)
	})

	// POST method --------------------------------------------------------------------
	r.POST("/users", func(c *gin.Context) {
		var user model.User
		if err := c.ShouldBindJSON(&user); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		result := db.Create(&user)
		c.JSON(http.StatusOK, gin.H{"RowsAffected": result.RowsAffected})
	})

	// DELETE method ------------------------------------------------------------------
	r.DELETE("/users/:id", func(ctx *gin.Context) {
		id := ctx.Param("id")
		var user model.User
		db.First(&user, id)
		db.Delete(&user)
		ctx.JSON(http.StatusOK, user)
	})

	// PUT method ---------------------------------------------------------------------
	r.PUT("/users", func(ctx *gin.Context) {
		var user model.User // user อันนี้ส่งมาจาก req ของ api
		var updatedUser model.User
		if err := ctx.ShouldBindJSON(&user); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		db.First(&updatedUser, user.ID)
		updatedUser.Fname = user.Fname
		updatedUser.Lname = user.Lname
		updatedUser.Username = user.Username
		updatedUser.Avatar = user.Avatar
		db.Save(updatedUser)
		ctx.JSON(http.StatusOK, updatedUser)
	})

	r.Use(cors.Default()) // CORs middleware สำหรับเรียกใช้ข้าม domain ได้
	r.Run()               // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
